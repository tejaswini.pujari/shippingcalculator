import React from "react";
import { Form, Input, Select, Button, Row, Card, Col, Divider } from "antd";
import "./App.css";
import DropDown from "./DropDown";

const { Option } = Select;
export default function CalculatorForm() {
  return (
    <Form
      id="form"
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      autoComplete="off"
    >
      <Row>
        <Col span={24}>
          <b>Pincode</b>
        </Col>
        <Col span={12}>
          <Form.Item
            label="Pickup"
            name="PickupPincode"
            rules={[
              {
                required: true,
                message: "Please input your PickupPincode!",
              },
            ]}
            style={{ display: "inline" }}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            label="Drop"
            name="DropPincode"
            rules={[
              {
                required: true,
                message: "Please input your DropPincode!",
              },
            ]}
            style={{ display: "inline" }}
          >
            <Input />
          </Form.Item>
        </Col>
        <Divider />
        <Col span={24}>
          <b>Package Information</b>
        </Col>
        <Col span={12}>
          <Form.Item
            label="Weight"
            style={{ marginBottom: 0, display: "inline" }}
          >
            <Form.Item
              name="Weight"
              rules={[{ required: true }]}
              style={{
                display: "inline-block",
                width: "calc(30% - 5px)",
                marginRight: "0.5rem",
              }}
            >
              <Input placeholder="00" />
            </Form.Item>
            <Form.Item
              name="Package Weight"
              style={{
                display: "inline-block",
                width: "calc(30% - 5px)",
              }}
            >
              <Select placeholder="Select Unit">
                <Option Value="Kg">Kg</Option>
                <Option Value="Hg">Hg</Option>
                <Option Value="Gm">gm</Option>
              </Select>
            </Form.Item>
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            label="Dimension"
            style={{ marginBottom: 0, display: "inline" }}
          >
            <Form.Item
              name="Length"
              rules={[{ required: true }]}
              style={{
                display: "inline-block",
                width: "calc(25% - 5px)",
                marginRight: "0.5rem",
              }}
            >
              <Input placeholder="Length" />
            </Form.Item>
            <Form.Item
              name="Breadth"
              rules={[{ required: true }]}
              style={{
                display: "inline-block",
                width: "calc(25% - 8px)",
                marginRight: "0.5rem",
              }}
            >
              <Input placeholder="Breadth" />
            </Form.Item>
            <Form.Item
              name="Height"
              rules={[{ required: true }]}
              style={{
                display: "inline-block",
                width: "calc(25% - 8px)",
                marginRight: "0.5rem",
              }}
            >
              <Input placeholder="Height" />
            </Form.Item>

            <Form.Item
              name="Dimension"
              style={{
                display: "inline-block",
                width: "calc(25% - 5px)",
                BgColorsOutlined: "red",
              }}
            >
              <Select placeholder="Select">
                <Option Value="Kg">CM</Option>
                <Option Value="Hg">KM</Option>
              </Select>
            </Form.Item>
          </Form.Item>
        </Col>
        <Divider />

        <Col span={12}>
          <Form.Item
            label="Payment Mode"
            style={{ marginBottom: 0, display: "inline" }}
          >
            <Form.Item
              name="payment"
              rules={[{ required: true }]}
              style={{
                display: "inline-block",
                width: "calc(100% - 5px)",
                marginRight: "0.5rem",
              }}
            >
              <Select placeholder="Select ">
                <Option>COD</Option>
                <Option>Online</Option>
              </Select>
            </Form.Item>
          </Form.Item>
        </Col>

        <Col span={12}>
          <Form.Item
            label="Invoice Value"
            style={{ marginBottom: 0, display: "inline" }}
          >
            <Form.Item
              name="Invoice Value"
              rules={[{ required: true }]}
              style={{
                display: "inline-block",
                width: "calc(100% - 5px)",
                marginRight: "0.5rem",
              }}
            >
              <Input />
            </Form.Item>
          </Form.Item>
        </Col>

        <Col span={24} id="btn">
          <Button type="primary" htmlType="submit" justify>
            Submit
          </Button>
        </Col>
      </Row>
    </Form>
  );
}
