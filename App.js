import "./App.css";
import ShippingCalculator from "./ShippingCalculator";

function App() {
  return (
    <div className="App">
      <ShippingCalculator />
    </div>
  );
}

export default App;
