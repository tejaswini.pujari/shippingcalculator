import React, { Component } from "react";
import { Form, Input, Select, Button, Row, Card, Col, Divider } from "antd";
import "./App.css";
import DropDown from "./DropDown";
import CalculatorForm from "./CalculatorForm";

const { Option } = Select;
export class ShippingCalculator extends Component {
  render() {
    return (
      <Card id="card" style={{ margin: "2rem" }}>
        <Card style={{ fontSize: "20px", fontFamily: "cursive" }}>
          <b>Shipping Rate Calculator</b>
        </Card>
        <Card style={{ marginTop: 16 }} id="calculator">
          <CalculatorForm />
        </Card>
      </Card>
    );
  }
}

export default ShippingCalculator;


